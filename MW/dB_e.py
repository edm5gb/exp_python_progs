#!/usr/bin/python3
from math import *
from numpy import *

print('\nattn')
attn  = arange(0,50*2+1)/2.0
for a in attn:
    field = 7.58e-6 + 17.05*exp( -(a - 21.2)/8.69 )
    print(a, '\t', field)

print('\nFields')
field = arange(1,20)
for f in field:
    attn  = 21.2 - 8.69*log( (f - 7.58e-6)/17.05 )
    print(f, '\t', attn)