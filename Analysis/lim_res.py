#!/usr/bin/python3
from numpy import *
from matplotlib.pyplot import *
import datetime

from toolkit_eric import *

# Make a plot showing the limit and MW resonances.
# Boring, but quality of both show how trustworthy data is.

figure( 1, figsize = (7.5,10) )
# -----------------------------------------------------------------------------
# Load MW
# (0 i, 1 frequency, 2 norm, 3 norm bkgnd, 4 signal, 5 signal bkgnd)
n = 2
data = lin_ave( loadtxt( '2_32dB.txt' ), n )
# -----------------------------------------------------------------------------
# Normalization and Signal

freq   = data[:,1]
norm   = data[:,2] - data[:,3]
signal = data[:,4] - data[:,5]
normal = signal/norm

# Plotting
subplot(212)
plot( freq, normal, 'k-', label = '5 V/cm' )

xlim( l_lim( xlim() ) )
ylim( l_lim( ylim() ) )
grid(True)
xlabel('Frequency (GHz)')
ylabel('Normalized Signal')
legend()

# -----------------------------------------------------------------------------
# Load Limit
# (0 i, 1 frequency, 2 norm, 3 norm bkgnd, 4 signal, 5 signal bkgnd)
n = 2
data = lin_ave( loadtxt( '1_limit.txt' ), n )
# -----------------------------------------------------------------------------
# Normalization and Signal

freq   = data[:,1]
norm   = data[:,2] - data[:,3]
signal = data[:,4] - data[:,5]
normal = signal/norm

# Plotting
subplot(211)
plot( freq, normal, 'k-', label = '0 V/cm' )

xlim( l_lim( xlim() ) )
ylim( l_lim( ylim() ) )
grid(True)
# xlabel('Frequency (GHz)')
ylabel('Normalized Signal')
legend()

title( datetime.date.today().isoformat() + '\nDL-Pro Scans at 0 and 5 V/cm' )

show()
# savefig('lim_res.pdf')