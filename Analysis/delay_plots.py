#!/usr/bin/python3
from numpy import *
from scipy import special, optimize
from matplotlib.pyplot import *

from toolkit_eric import *

def delay_plots( filename, mw_freq, lab, format, offset):
    # Make plots of normalized and fitted delay scan data.
    
    wl = 0.5 # Updated to work with fixed wavelength CosFit()
    
    # Parameters
    fit_print(filename, mw_freq, 1)

    #Load Data
    data = loadtxt(filename)
    filename = filename.replace('.txt', '_bkgnd.txt')
    data_b = loadtxt(filename)

    # Break Down Data
    steps  = data[:,1]
    delay  = mw_phase(steps, mw_freq)
    norm   = data[:,2] - data[:,3]
    signal = data[:,4] - data[:,5]
    normal = signal/norm

    # Generate a fit
    fitfunc = lambda p, delay: p[0] + p[1]*cos( 2*pi*(delay - p[2]) / wl ) # Target function
    p, p_unc =  CosFit(delay,normal,[0.0,0.0,0.0])

    # Normalized & Fit Plot
    # figure(1, figsize = (10,7.5) )
    # subplot(142)
    plot( delay, normal + offset, 'k-', color = '0.5' )
    plot( delay, fitfunc(p, delay) + offset, format, linewidth = 3, label = lab )
    
    return

filenames = [ '3_Locked_32dB_p4.txt', '4_Locked_29dB_p4.txt' ]
labels    = [ '5 V/cm, +4 & +32 GHz', '7 V/cm, +4 & +32 GHz' ]
offsets   = [  0.0,                   0.0 ]
formats   = [ 'k-',                   'k-']
mw_freqs  = [ 14008.0e6,             14008.0e6 ]

figure( 1, figsize = (7.5,10) )
for i in arange( 0, len(filenames) ):
    delay_plots( filenames[i], mw_freqs[i], labels[i], formats[i], offsets[i] )

# axvline( 0.070+0.5, 0, 1, 'k-', linewidth = 3 )
xlim( [0,2] )
ylim( [0,1] )
grid(True)
xlabel('Delay (MW Wavelength)')
ylabel('Normalized Signal')
title('2014-12-03\nDelay Scans')

# show()
savefig('delay_plots.pdf')
