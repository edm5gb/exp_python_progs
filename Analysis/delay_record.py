#!/usr/bin/python3
from numpy import *
from scipy import special, optimize
from matplotlib.pyplot import *

from toolkit_eric import *

def drec_write(datafile, n, fitfile, print_label, date, l_freq, mw_attn, mw_freq, signalshare):
    # Given delay scan data, normalize, fit and then write a report.
    
    wl = 0.5 # Updated to use fixed wavelength CosFit()
    
    # Analyze data
    delay, normal = analyze(datafile, mw_freq, n)
    fitfunc = lambda p, delay: p[0] + p[1]*cos( 2*pi*(delay - p[2]) / wl ) # Target function
    p, p_unc =  CosFit(delay,normal,[0.0,0.0,0.0]) # Fit paramters and errors
    
    # Build header
    str_l_freq  = '%.1f\t' % (l_freq)
    str_mw_attn = '%.1f\t' % (mw_attn)
    str_mw_freq = '%.1f\t' % (mw_freq*1e-6)
    
    data_head = date     + '\t' + datafile + '\t' + str_l_freq        + str_mw_attn    + str_mw_freq
    labl_head = '0 date' + '\t' + '1 file' + '\t' + '2 laser freq \t' + '3 MW Attn \t' + '4 MW freq \t'
    
    # Build signalshare
    data_sig = '%.3f\t%.3f\t%.3f\t' % (signalshare[0], signalshare[1], signalshare[2])
    labl_sig = '5 signal low \t 6 signal high \t 7 signal both \t'
    
    # Build fit string
    data_mean = '%.2e\t%.2e\t' % ( p[0], p_unc[0] )
    data_amp  = '%.2e\t%.2e\t' % ( p[1], p_unc[1] )
    data_phi  = '%.3f\t%.3f\t' % ( p[2], p_unc[2] )
    data_wln  = '%.3f\t%.3f\t' % ( wl,   0.0 ) # Updated to use fixed wavelength CosFit()
    cont     = abs( p[1]/p[0] )
    cont_unc = ( (1/p[0])**2 * p_unc[1]**2 + (p[1]/p[0]**2)**2 * p_unc[0]**2 )**(0.5)
    data_cont = '%.3f\t%.3f\t' % ( cont, cont_unc )
    
    data_fit  = data_mean              + data_amp                      + data_phi                  + data_wln                       + data_cont
    labl_fit  = '8 mean \t 9 sigma \t' + '10 amplitude \t 11 sigma \t' + '12 phase \t 13 sigma \t' + '14 wavelength \t 15 sigma \t' + '16 contrast \t 17 sigma \t'
    
    # Write info to fitfile
    f = open(fitfile, 'a')
    if print_label == True:
        f.write('# ' + labl_head + labl_sig + labl_fit + '\n')
    f.write(data_head + data_sig + data_fit + '\n')
    f.close()
    
    return

def delay_record():
    # Input all the interesting info that goes with each data file.
    date = '2014-12-03'
    datafiles = [ '3_Locked_32dB_p4.txt', '4_Locked_29dB_p4.txt' ]
    l_freq    = [ 4.0,                    4.0 ]
    mw_freqs  = [ 14008.0e6,              14008.0e6 ]
    mw_attns  = [ 32.0,                   29.0 ]
    sigshares = [ [0.180, 0.031, 0.120],  [0.107, 0.059, 0.090] ]
    n = 1
    # Make a new empty fitfile
    fitfile = 'delay_record.txt'
    f = open(fitfile, 'w')
    f.close()
    
    # Cycle through each datafile & parameters, use drec_write() to produce a report.
    for i in arange( 0, len(datafiles) ):
        # Only print the column label on the first line.
        if i == 0:
            print_label = True
        else:
            print_label = False
            
        drec_write(datafiles[i], n, fitfile, print_label, date, l_freq[i], mw_attns[i], mw_freqs[i], sigshares[i])
    
    return

delay_record()