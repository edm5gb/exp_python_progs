#!/usr/bin/python3
from numpy import *
from scipy import special, optimize
from matplotlib.pyplot import *

from toolkit_eric import *

def fprint(filename, mw_freq, n):
    # Print the fit parameters of a delay scan.
    # Plot the data and the fit.
    
    wl = 0.5 # Updated to use fixed wavelength CosFit()
    
    delay, normal = analyze(filename, mw_freq, 1)
    fitfunc = lambda p, delay: p[0] + p[1]*cos( 2*pi*(delay - p[2]) / wl ) # Target function
    p, p_unc =  CosFit(delay,normal,[0.0,0.0,0.0]) # Fit paramters and errors
    
    str_mean = '\n\t mean       = %.2e +/- %.2e' % ( p[0], p_unc[0] )
    str_amp  = '\n\t amplitude  = %.2e +/- %.2e' % ( p[1], p_unc[1] )
    cont = abs( p[1]/p[0] )
    cont_unc = ( (1/p[0])**2 * p_unc[1]**2 + (p[1]/p[0]**2)**2 * p_unc[0]**2 )**(0.5)
    str_cont = '\n\t contrast   = %.3f +/- %.3f' % ( cont, cont_unc )
    str_wln  = '\n\t wavelength = %.3f +/- %.3f' % ( wl , 0.0 ) # Updated to use fixed wavelength CosFit()
    str_phi  = '\n\t phase      = %.3f +/- %.3f' % ( p[2], p_unc[2] )
    str_fit  = str_mean + str_amp + str_cont + str_wln + str_phi
    
    print('\n' + filename + ' ' + 20*'-' + str_fit)
    
    # plot(delay, normal, label = filename)
    # plot(delay, fitfunc(p, delay), linewidth = 3)
    
    return

def check_delay():
    # After delay scan data has been collected, quickly check that it's decent.
    # Gives plots of
    #   (1) Background subtracted signal - is the raw signal okayish?
    #   (2) Normalized signal & fit - Is the data and fit decent?
    #   (3) Fit residuals - are there weird patterns?
    #   (4) Background data - Is the background level well behaved through the data collection?

    # Parameters
    filename = '3_Locked_32dB_p4.txt'
    mw_freq = 14008.2e6
    wl = 0.5 # Updated to use fixed wavelength CosFit()
    fprint(filename, mw_freq, 1)

    #Load Data
    data = loadtxt(filename)
    filename = filename.replace('.txt', '_bkgnd.txt')
    data_b = loadtxt(filename)

    # Break Down Data
    steps  = data[:,1]
    delay  = mw_phase(steps, mw_freq)
    norm   = data[:,2] - data[:,3]
    signal = data[:,4] - data[:,5]
    normal = signal/norm

    # Generate a fit
    fitfunc = lambda p, delay: p[0] + p[1]*cos( 2*pi*(delay - p[2]) / wl ) # Target function
    p, p_unc =  CosFit(delay,normal,[0.0,0.0,0.0])

    # Signal Plot
    figure(1)
    # subplot(141)
    plot( delay, signal, label = 'signal'  )
    xlim( l_lim( xlim() ) )
    ylim( l_lim( ylim() ) )
    grid(True)
    xlabel('Delay (MW Wavelengths)')
    ylabel('e- Signal (Arb. U.)')
    legend()

    # Normalized & Fit Plot
    figure(2)
    # subplot(142)
    plot( delay, normal, 'b-', label = 'norm' )
    plot( delay, fitfunc(p, delay), 'k-', linewidth = 3)
    xlim( l_lim( xlim() ) )
    ylim( l_lim( ylim() ) )
    grid(True)
    xlabel('Delay (MW Wavelength)')
    ylabel('Normalized Signal')
    legend()

    # Residuals
    figure(3)
    # subplot(143)
    plot( delay, normal - fitfunc(p, delay), label = 'residual' )
    xlim( l_lim( xlim() ) )
    ylim( l_lim( ylim() ) )
    grid(True)
    xlabel('Delay (MW Wavelength)')
    ylabel('Residual')
    legend()

    # Background Plots
    figure(4)
    # subplot(144)
    plot( data_b[:,0], data_b[:,1], label = 'norm bkgnd')
    plot( data_b[:,0], data_b[:,2], label = 'signal bkgnd')
    xlim( l_lim( xlim() ) )
    ylim( l_lim( ylim() ) )
    grid(True)
    xlabel('i')
    ylabel('e- Signal (Arb. U.)')
    legend()

    show()
    
    return

check_delay()