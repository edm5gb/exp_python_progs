#!/usr/bin/python3
from numpy import *
from matplotlib.pyplot import *

from toolkit_eric import *

# Check frequency scan data

#------------------------------------------------------------------------------    
# Load Data
# (0 i, 1 frequency, 2 norm, 3 norm bkgnd, 4 signal, 5 signal bkgnd)
filename  = '1_limit.txt'
n = 2
data   = lin_ave( loadtxt(filename), n )
# (0 signal bkgnd, 1 norm bkgnd, 2 norm)
filename = filename.replace('.txt', '_bkgnd.txt')
data_b = loadtxt(filename)
#------------------------------------------------------------------------------    
# Normalization and Signal
figure(1)

trim = 0
points = arange(0, len(data[:,0]) - trim)
freq   = data[:,1][points]
norm   = ( data[:,2] - data[:,3] )[points]
signal = ( data[:,4] - data[:,5] )[points]

# subplot(131)
plot( freq, signal, label = 'signal'  )
xlim( l_lim( xlim() ) )
ylim( l_lim( ylim() ) )
grid(True)
xlabel('Frequency (GHz)')
ylabel('e- Signal (Arb. U.)')
legend()

figure(2)
# subplot(132)
plot( freq, signal/norm, label = 'norm' )
xlim( l_lim( xlim() ) )
#xlim( [-30,0] )
ylim( l_lim( ylim() ) )
grid(True)
xlabel('Frequency (GHz)')
ylabel('e- Signal (Arb. U.)')
legend()

figure(3)
# subplot(133)
# plot( data_b[:,0], label = 'signal bkgnd')
plot( data_b[:,0], data_b[:,1], label = 'norm bkgnd')
plot( data_b[:,0], data_b[:,2], label = 'signal bkgnd')
grid(True)
xlabel('i')
ylabel('e- Signal (Arb. U.)')
legend()

show()