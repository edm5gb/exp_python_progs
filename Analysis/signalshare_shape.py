#!/usr/bin/python3
from math import*
from numpy import *
from matplotlib.pyplot import *

from toolkit_eric import *

def signalshare_normal(filename):
    # Take the stupidly shaped signalshare files and make them less stupid
    # -----
    # filename : signalshare file of the format
    # rows  1-10 : (0 norm bkgnd | 1 signal bkgnd)
    # rows 11-20 : (0 norm       | 1 signal )
    # rows 21-30 : (0 norm bkgnd | 1 signal bkgnd)
    # rows 31-40 : (0 norm       | 1 signal )
    
    data = loadtxt(file)
    datanew = zeros([2,6])
    datanew[:,0] = [0,1] # iter
    datanew[:,1] = [0,1] # freq
    
    datanew[0,3] = mean( data[ arange( 0,10), 0] ) # norm   bkgnd
    datanew[0,5] = mean( data[ arange( 0,10), 1] ) # signal bkgnd
    datanew[0,2] = mean( data[ arange(10,20), 0] ) # norm
    datanew[0,4] = mean( data[ arange(10,20), 1] ) # signal
    
    datanew[1,3] = mean( data[ arange(20,30), 0] ) # norm   bkgnd
    datanew[1,5] = mean( data[ arange(20,30), 1] ) # signal bkgnd
    datanew[1,2] = mean( data[ arange(30,40), 0] ) # norm
    datanew[1,4] = mean( data[ arange(30,40), 1] ) # signal
    
    norm   = datanew[:,2] - datanew[:,3]
    signal = datanew[:,4] - datanew[:,5]
    normal = signal/norm
    
    return mean(normal)

# Analyze
filename = '4_signalshare_29dB_p4.txt'
filelist = [filename, filename.replace('p4.txt', 'p32.txt'), filename.replace('p4.txt', 'p4p32.txt')]
shares = zeros(3)
for i in [0,1,2]:
    file = filelist[i]
    share = signalshare_normal(file)
    shares[i] = share
    print(file)
    print( share, '\n')

# Plot
plot( [0,0,0], shares, 'k.', label = 'measured' )
plot( 0, (shares[0] + shares[1]) / 2, 'r.', label = '(hi + lo) / 2' )
ylim( [0, l_lim( ylim() )[1] ] )
legend()
title( 'Signalshares, Unlocked vs Expected' )
show()

# Write
outfile = filename.replace('_p4.txt', '.txt')
f = open(outfile, 'w')
f.write( '# ' + filelist[0] + '\t' + filelist[1] + '\t' + filelist[2]+ '\t' + '\n')
f.write( str(shares[0]) + '\t' + str(shares[1]) + '\t' + str(shares[2]) )
f.close()