#!/usr/bin/python3
from numpy import *
from scipy import special, optimize
from matplotlib.pyplot import *

from toolkit_eric import *

def drec_analysis( rec_filename ):
    # Given a record of delay scans produced by delay_record.py
    # extract information into arrays and plot.
    # Return mw field and contrast arrays
    # -----
    # rec_filename : a record file produced by delay_record.py of the format
    # (0 file | 1 date | 2 laser freq | 3 MW Attn | 4 MW freq |
    # | 5 signal low | 6 signal high | 7 signal both |
    # | 8 mean | 9 sigma | 10 amplitude | 11 sigma | 12 phase | 13 sigma | 14 wavelength | 15 sigma | 16 contrast | 17 sigma )

    # Read the text into a nested list to pick apart later
    data = []
    f = open(filename, 'r')
    for line in f.readlines():
        if line[0] != '#':
            data.append( line.split('\t') )
    f.close()

    # All of the interesting arrays
    l = len(data)
    names    = empty( l, dtype = str)
    dates    = empty( l, dtype = str)
    l_freqs  = zeros( l )
    mw_attns = zeros( l )
    mw_flds  = zeros( l )
    mw_freqs = zeros( l )
    sigs     = zeros( [3,l] )
    means    = zeros( [2,l] )
    amps     = zeros( [2,l] )
    phis     = zeros( [2,l] )
    wlns     = zeros( [2,l] )
    conts    = zeros( [2,l] )

    # Pull apart data and put the info into the appropriate arrays
    for i in arange(0,l):
        dates[i]    = data[i][0] # YYYY-MM-DD
        names[i]    = data[i][1] # filenames of delay data
        l_freqs[i]  = data[i][2] # GHz above limit
        mw_attns[i] = data[i][3] # dB on Variable Attenuator
        mw_flds[i]  = field( mw_attns[i] ) # V/cm
        mw_freqs[i] = data[i][4] # Frequencies in MHz
        sigs[:,i]   = [data[i][5], data[i][6], data[i][7]]
        means[:,i]  = [ data[i][ 8], data[i][ 9]  ] # Normalized signal
        amps[:,i]   = [ data[i][10], data[i][11]  ] # Normalized signal
        phis[:,i]   = [ data[i][12], data[i][13] ] # In fit wavelengths (~2x mw wavelength)
        wlns[:,i]   = [ data[i][14], data[i][15] ] # Fit wavelengths (~2x mw wavelength
        conts[:,i]  = [ data[i][16], data[i][17] ] # Michaelson Contrast
    
    # Plot interesting relationships
    figure(1, figsize = (7.5,10) )
    isort = mw_flds.argsort()
    
    # Offsets vs MW Field
    subplot(311)
    plot( mw_flds, phis[0,:], 'k.' )
    errorbar( mw_flds, phis[0,:], yerr = phis[1,:], linestyle = "None", ecolor = 'k' )
    xlim( [0,20] )
    ylim( [-0.5,0.5] )
    grid(True)
    # xlabel( 'MW Field (V/cm)' )
    ylabel( 'Peak Phase (Beat Note Wavelengths)' )
    title('2014-12-03 \nLasers at +4 GHz and +32 GHz')
    
    # Amplitude vs MW Field
    subplot(312)
    plot( mw_flds, amps[0,:]), 'k.' )
    errorbar( mw_flds, amps[0,:], yerr = amps[1,:], linestyle = "None", ecolor = 'k' )
    xlim( [0,20] )
    ylim( [0,0.1] )
    grid(True)
    # xlabel( 'MW Field (V/cm)' )
    ylabel( 'Amplitude' )
    
    # Signals vs MW Field
    subplot(313)
    plot( mw_flds[isort], sigs[0,:][isort], 'b>-', label = '+4 GHz')
    plot( mw_flds[isort], sigs[1,:][isort], 'g<-', label = '+32 GHz')
    plot( mw_flds[isort], sigs[2,:][isort], 'r^-', label = 'both')
    plot( mw_flds, means[0,:], 'k.', label = 'locked' )
    errorbar( mw_flds, means[0,:], yerr = means[1,:], linestyle = "None", ecolor = 'k' )
    xlim( [0,20] )
    ylim( [0,0.5] )
    grid(True)
    xlabel( 'MW Field (V/cm)' )
    ylabel( 'Unlocked Signal' )
    legend()
    
    show()
    # savefig('record_analysis.pdf')
    
    return mw_flds, conts

filename = 'delay_record.txt'
drec_analysis(filename)