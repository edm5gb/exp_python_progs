#!/usr/bin/python3
import sys
import os
from pylab import *
from scipy import special, optimize

def zero_offset(filename):
    # Use a data file to define the "zero" of the signal
    data = loadtxt(filename)
    offset = mean(data[:,1])
    return offset

def CosFit(x,y,ini_guess=[0.0,0.0,0.0,0.0]):
    # Fit dataset with a cosine function defined by : y -> p[0]+p[1]*cos(2*pi*(x-p[2])/p[3])
    # dataset : Datset as created by the load_data function
    # ini_guess : Vector contaning the intial set of fitting parameters, if empty the function
    # will make a guess.
    #from scipy import optimize
    
    if allclose(ini_guess,[0.0,0.0,0.0,0.0]):
        ini_guess[0] = y.mean()
        ini_guess[1] = 0.5*(y.max()-y.min())
        ini_guess[2] = 0.0
        ini_guess[3] = 0.5
        
        
    
    fitfunc = lambda p, x: p[0]+p[1]*cos(2*pi*(x-p[2])/p[3]) # Target function
    errfunc = lambda p, x, y: fitfunc(p, x) - y # Distance to the target function
    
    p1, success = optimize.leastsq(errfunc, ini_guess[:], args=(x, y))
    
    if abs(p1[3]-0.5) > 0.15:
        print('More than 15\% on the wavelength, check the fit !')
        
    return p1


def delay(y,fmw = 14.0, steps=20000.0, pitch = 2.032095e-4):
    # Compute the delay introduced by the delay line in unit od MW wavelength
    # y : Stage travel in units of stepper motor steps
    # fmw : MW frequency in GHz
    # steps : Number of steps per revolution of the stepper motor
    # pitch : Pitch of the ball screw in mm/turn
    n=1.00027493 # air optical index @ 819 nm
    c=299792458 # speed of ligth in vacuum (m/s)
    lmw = c/(n*fmw*1e9) # Microwave wavelength (m)
    delay = 2*y/steps*pitch*1e-3/lmw
    return delay


def main():
    #filename = sys.argv[1]
    
    # Set the proper zero offset for the data
    offset = zero_offset("29_Zero.txt") # Zero datafile for 2014-10-30
    print('offset = ', offset)
    
    # List of files to fit
    # All files
    filelist = os.listdir('.')
    remlist = []
    # List of bad files (No 'Lock' or '.txt')
    print('\n', 'Files in folder')
    for name in  filelist:
        print(name)
        if (name.find('Lock')==-1 or name.endswith('.txt')==False):
            print('\t |-> ignored')
            remlist.append(name)
    # Remove these files
    for name in remlist:
        filelist.remove(name)
    # Sort
    filelist.sort()        
    # File to write fit parameters to
    outfile = 'fitmod.txt'
    f = open(outfile, 'w')
    f.write('Filename \t Mean \t Amplitude \t x Offset \t Wavelength \n')
    
    # Do fitting on remaining files, make data & fit PDFs
    print('\n', 'Fit Parameters:')
    print('Name \t [Mean, Amplitude, x Offset, Wavelength]')
    for filename in  filelist :
        data = loadtxt(filename)
        fmw = 14.0083 #MW frequency (GHz)
        
        #offset = -0.034312168739999968 # Offset correction for 2014-10-16 data
        
        # fit the data
        fitfunc = lambda p, x: p[0]+p[1]*cos(2*pi*(x-p[2])/p[3]) # Target function
        #x = delay(data[:,0],fmw,20000.0,4.08)
        x = delay(data[:,0],fmw,1,2.032095e-4)
        p =  CosFit(x,data[:,1]-offset,[0.0,0.0,0.0,0.0])
        # Write filename, mean, amplitude, x offset, wavelength
        f.write(filename + '\t' + str(p[0]) + '\t' + str(p[1]) + '\t' + str(p[2]) + '\t' + str(p[3]) + '\n')
        print(filename+'\t'+array_str(p))
        
        #Make plot figure
        clf()
        # Add fit data to title
        subtitle = 'mean :%.2e ampl : %.2e contrast : %.1f%%' % (p[0], p[1], abs(p[1]/p[0]*100.0))
        title(filename + '\n' + subtitle)
        xlabel('Delay (MW wavelength)')
        ylabel('Electron signal (Arb. unit)')
        grid(True)
        plot(x,data[:,1]-offset)
        plot(x,fitfunc(p,x),linewidth=3, linestyle='--',c='red')
#        # Put fit data on figure
#        text(0.1,(data[:,1]-offset).max()+0.01,'mean :%.2e ampl : %.2e contrast : %.1f%%'%(p[0],p[1],abs(p[1]/p[0]*100.0)))
        # Save figure to PDF
        savefig(filename.replace('.txt','.pdf'),orientation='landscape',papertype='letter')
    
    f.close
    
main()
